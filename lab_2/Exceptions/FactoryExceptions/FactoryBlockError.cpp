//
// Created by Elizaveta on 11.12.2021.
//

#include <cstring>
#include "FactoryBlockError.h"
#include "../../Constants.h"

namespace Exceptions {

    FactoryBlockError::FactoryBlockError(const std::string &blockName, const std::string &m) : blockName(blockName), message(m) {
        message = message + blockName + '!';
    }

    char const *FactoryBlockError::what() const noexcept {
        return message.c_str();
    }
}