//
// Created by Elizaveta on 11.12.2021.
//

#ifndef LAB_2_FACTORYBLOCKERROR_H
#define LAB_2_FACTORYBLOCKERROR_H

#include <exception>
#include <string>

namespace Exceptions {

class FactoryBlockError : public std::exception {
    private:
        std::string message;
        std::string blockName;
    public:
        explicit FactoryBlockError(const std::string &blockName, const std::string &m = "Error in factory with block ");

        char const *what() const noexcept override;
    };
}

#endif //LAB_2_FACTORYBLOCKERROR_H
