//
// Created by Elizaveta on 04.12.2021.
//

#ifndef LAB_2_CREATEUNRECOGNISEDBLOCK_H
#define LAB_2_CREATEUNRECOGNISEDBLOCK_H

#include <exception>
#include <string>
#include "FactoryBlockError.h"

namespace Exceptions {

    class CreateUnrecognisedBlock : public FactoryBlockError {
    public:
        explicit CreateUnrecognisedBlock(const std::string &blockName, const std::string &m = "Unrecognised block");
    };
}

#endif //LAB_2_CREATEUNRECOGNISEDBLOCK_H
