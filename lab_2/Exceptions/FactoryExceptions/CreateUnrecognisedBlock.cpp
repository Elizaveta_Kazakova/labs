//
// Created by Elizaveta on 04.12.2021.
//

#include "CreateUnrecognisedBlock.h"

namespace Exceptions {

    CreateUnrecognisedBlock::CreateUnrecognisedBlock(const std::string &blockName, const std::string &m) : FactoryBlockError(blockName, m) { }

}