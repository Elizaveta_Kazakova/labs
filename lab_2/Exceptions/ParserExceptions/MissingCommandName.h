//
// Created by Elizaveta on 04.12.2021.
//

#ifndef LAB_2_MISSINGCOMMANDNAME_H
#define LAB_2_MISSINGCOMMANDNAME_H

#include <string>
#include "TooFewArguments.h"

namespace Exceptions {

    class MissingCommandName : public TooFewArguments {
    public:
        explicit MissingCommandName(size_t line, const std::string &m = "Missing command name on line ");
    };
}

#endif //LAB_2_MISSINGCOMMANDNAME_H
