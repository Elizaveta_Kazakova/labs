//
// Created by Elizaveta on 04.12.2021.
//

#include "MissingCommandName.h"

namespace Exceptions {

    MissingCommandName::MissingCommandName(const size_t line, const std::string &m) :
        TooFewArguments(m + std::to_string(line) + '!'){
    }

}