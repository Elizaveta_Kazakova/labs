//
// Created by Elizaveta on 04.12.2021.
//

#include "EqualityMoreThanOnce.h"

namespace Exceptions {

    EqualityMoreThanOnce::EqualityMoreThanOnce(const std::string &m) : message(m) { }

    char const *EqualityMoreThanOnce::what() const noexcept {
        return message.c_str();
    }

}