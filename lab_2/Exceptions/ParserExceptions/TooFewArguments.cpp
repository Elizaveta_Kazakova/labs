//
// Created by Elizaveta on 04.12.2021.
//

#include <cstring>
#include "TooFewArguments.h"

namespace Exceptions {

    TooFewArguments::TooFewArguments(const std::string &m) : message(m) {    }

    char const *TooFewArguments::what() const noexcept {
        return message.c_str();
    }
}