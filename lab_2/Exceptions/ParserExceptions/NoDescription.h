//
// Created by Elizaveta on 05.12.2021.
//

#ifndef LAB_2_NODESCRIPTION_H
#define LAB_2_NODESCRIPTION_H

#include <exception>
#include <string>

namespace Exceptions {

class NoDescription : public std::exception {
    private:
    const std::string message;
    public:
        explicit NoDescription(const std::string &m = "Missing description!");

       char const *what() const noexcept override;

    };
}

#endif //LAB_2_NODESCRIPTION_H
