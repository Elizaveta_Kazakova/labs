//
// Created by Elizaveta on 04.12.2021.
//

#ifndef LAB_2_EQUALITYMORETHANONCE_H
#define LAB_2_EQUALITYMORETHANONCE_H

#include <exception>
#include <string>

namespace Exceptions {

class EqualityMoreThanOnce : public std::exception {
    private:
         const std::string message;
    public:
        explicit EqualityMoreThanOnce(const std::string &m  = "Equal sign more than once!");

        char const *what() const noexcept override;

    };
}

#endif //LAB_2_EQUALITYMORETHANONCE_H
