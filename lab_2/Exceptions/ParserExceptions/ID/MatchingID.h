//
// Created by Elizaveta on 24.12.2021.
//

#ifndef LAB_2_MATCHINGID_H
#define LAB_2_MATCHINGID_H


#include "IncorrectID.h"

namespace Exceptions {

    class MatchingID : public IncorrectID {
    public:
        explicit MatchingID(const std::string &m = "Two or more usage of one id!");
    };
}


#endif //LAB_2_MATCHINGID_H
