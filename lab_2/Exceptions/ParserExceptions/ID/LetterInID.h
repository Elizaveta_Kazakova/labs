//
// Created by Elizaveta on 04.12.2021.
//

#ifndef LAB_2_LETTERINID_H
#define LAB_2_LETTERINID_H


#include "IncorrectID.h"

namespace Exceptions {

    class LetterInID : public IncorrectID {
    public:
        explicit LetterInID(const std::string &m = "Letter in id!");
    };
}

#endif //LAB_2_LETTERINID_H
