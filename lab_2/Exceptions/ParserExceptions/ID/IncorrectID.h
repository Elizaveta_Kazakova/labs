#ifndef LAB_2_INCORRECTID_H
#define LAB_2_INCORRECTID_H

#include <exception>
#include <string>

namespace Exceptions {

class IncorrectID : public std::exception {
    private:
    const std::string message;
    public:
        explicit IncorrectID(const std::string &m = "ID is incorrect");

        char const *what() const noexcept override;

    };
}

#endif //LAB_2_INCORRECTID_H
