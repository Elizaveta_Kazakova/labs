//
// Created by Elizaveta on 01.12.2021.
//

#include "IncorrectID.h"

namespace Exceptions {

    IncorrectID::IncorrectID(const std::string &m) : message(m) { }

    char const *IncorrectID::what() const noexcept {
        return message.c_str();
    }


}