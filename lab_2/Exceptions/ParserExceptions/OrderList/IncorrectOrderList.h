//
// Created by Elizaveta on 05.12.2021.
//

#ifndef LAB_2_INCORRECTORDERLIST_H
#define LAB_2_INCORRECTORDERLIST_H

#include <exception>
#include <string>

namespace Exceptions {

class IncorrectOrderList : public std::exception {
    private:
    const std::string message;
    public:
        explicit IncorrectOrderList(const std::string &m  = "Incorrect order list!");

        char const *what() const noexcept override;

    };
}

#endif //LAB_2_INCORRECTORDERLIST_H
