//
// Created by Elizaveta on 06.12.2021.
//

#include "MissingOrderOfExe.h"

namespace Exceptions {

    MissingOrderOfExe::MissingOrderOfExe(const std::string &m ) : IncorrectOrderList(m) { }

}