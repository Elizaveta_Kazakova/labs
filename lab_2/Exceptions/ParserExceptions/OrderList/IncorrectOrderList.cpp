//
// Created by Elizaveta on 05.12.2021.
//

#include "IncorrectOrderList.h"

namespace Exceptions {

    IncorrectOrderList::IncorrectOrderList(const std::string &m) : message(m) {}

    char const *IncorrectOrderList::what() const noexcept {
        return message.c_str();
    }

}