//
// Created by Elizaveta on 06.12.2021.
//

#ifndef LAB_2_MISSINGORDEROFEXE_H
#define LAB_2_MISSINGORDEROFEXE_H

#include <exception>
#include <string>
#include "IncorrectOrderList.h"

namespace Exceptions {

    class MissingOrderOfExe : public IncorrectOrderList {
    public:
        explicit MissingOrderOfExe(const std::string &m  = "Missing order id!");
    };
}

#endif //LAB_2_MISSINGORDEROFEXE_H
