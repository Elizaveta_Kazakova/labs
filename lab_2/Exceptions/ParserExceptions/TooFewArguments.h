//
// Created by Elizaveta on 04.12.2021.
//

#ifndef LAB_2_TOOFEWARGUMENTS_H
#define LAB_2_TOOFEWARGUMENTS_H

#include <exception>
#include <string>

namespace Exceptions {

class TooFewArguments : public std::exception {
    private:
        const std::string message;
    public:
        explicit TooFewArguments(const std::string &m = "Too few arguments!");

        char const *what() const noexcept override;

    };
}

#endif //LAB_2_TOOFEWARGUMENTS_H
