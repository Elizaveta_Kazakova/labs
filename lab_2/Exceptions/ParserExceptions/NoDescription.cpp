//
// Created by Elizaveta on 05.12.2021.
//

#include "NoDescription.h"

namespace Exceptions {

    NoDescription::NoDescription(const std::string &m) : message(m) { }

    char const *NoDescription::what() const noexcept {
        return message.c_str();
    }
}