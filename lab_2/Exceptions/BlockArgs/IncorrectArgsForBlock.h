//
// Created by Elizaveta on 04.12.2021.
//

#ifndef LAB_2_INCORRECTARGSFORBLOCK_H
#define LAB_2_INCORRECTARGSFORBLOCK_H

#include <exception>
#include <string>

namespace Exceptions {

    class IncorrectArgsForBlock : std::exception {
    private:
        std::string message;
        std::string blockName;
    public:
        explicit IncorrectArgsForBlock(const std::string &blockName,
                                       const std::string &m = "Incorrect number of arguments in block ");

        char const *what() const noexcept override;
    };
}

#endif //LAB_2_INCORRECTARGSFORBLOCK_H
