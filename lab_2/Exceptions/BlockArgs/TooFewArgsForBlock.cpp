//
// Created by Elizaveta on 04.12.2021.
//

#include "TooFewArgsForBlock.h"

namespace Exceptions {

    TooFewArgsForBlock::TooFewArgsForBlock(const std::string &blockName, const std::string &m) : IncorrectArgsForBlock(blockName, m) { }
}