//
// Created by Elizaveta on 04.12.2021.
//

#ifndef LAB_2_TOOMANYARGSFORBLOCK_H
#define LAB_2_TOOMANYARGSFORBLOCK_H

#include <exception>
#include <string>
#include "IncorrectArgsForBlock.h"

namespace Exceptions {

    class TooManyArgsForBlock :public IncorrectArgsForBlock {
    public:
        explicit TooManyArgsForBlock(const std::string &blockName,const std::string &m = "Too many arguments for block ");
    };
}

#endif //LAB_2_TOOMANYARGSFORBLOCK_H
