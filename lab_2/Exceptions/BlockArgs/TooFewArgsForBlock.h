//
// Created by Elizaveta on 04.12.2021.
//

#ifndef LAB_2_TOOFEWARGSFORBLOCK_H
#define LAB_2_TOOFEWARGSFORBLOCK_H

#include <exception>
#include <string>
#include "IncorrectArgsForBlock.h"

namespace Exceptions {

    class TooFewArgsForBlock : public IncorrectArgsForBlock {
    public:
        explicit TooFewArgsForBlock(const std::string &blockName, const std::string &m = "Too few arguments for block ");
    };
}

#endif //LAB_2_TOOFEWARGSFORBLOCK_H
