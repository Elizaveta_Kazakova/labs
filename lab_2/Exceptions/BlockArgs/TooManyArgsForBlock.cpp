//
// Created by Elizaveta on 04.12.2021.
//

#include "TooManyArgsForBlock.h"

namespace Exceptions {

    TooManyArgsForBlock::TooManyArgsForBlock(const std::string &blockName, const std::string &m) : IncorrectArgsForBlock(blockName, m) { }
}