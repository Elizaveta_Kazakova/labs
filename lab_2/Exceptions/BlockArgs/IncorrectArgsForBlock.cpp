//
// Created by Elizaveta on 04.12.2021.
//

#include "IncorrectArgsForBlock.h"

namespace Exceptions {

    IncorrectArgsForBlock::IncorrectArgsForBlock(const std::string &blockName, const std::string &m) :
     blockName(blockName), message(m) {
        message = message + blockName + '!';
    }

    char const *IncorrectArgsForBlock::what() const noexcept {
        return message.c_str();
    }


}