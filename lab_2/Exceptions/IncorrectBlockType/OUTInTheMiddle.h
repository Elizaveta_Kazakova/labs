//
// Created by Elizaveta on 10.12.2021.
//

#ifndef LAB_2_OUTINTHEMIDDLE_H
#define LAB_2_OUTINTHEMIDDLE_H


#include "IncorrectBlockType.h"

namespace Exceptions {

    class OUTInTheMiddle : public IncorrectBlockType {
    public:
        explicit OUTInTheMiddle(const std::string &m = "Type OUT in the middle!");
    };
}

#endif //LAB_2_OUTINTHEMIDDLE_H
