//
// Created by Elizaveta on 10.12.2021.
//

#include "IncorrectBlockType.h"

namespace Exceptions {

    IncorrectBlockType::IncorrectBlockType(const std::string &m) : message(m) { }


    char const *IncorrectBlockType::what() const noexcept {
        return message.c_str();
    }

}