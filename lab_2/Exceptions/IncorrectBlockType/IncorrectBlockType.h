//
// Created by Elizaveta on 10.12.2021.
//

#ifndef LAB_2_INCORRECTBLOCKTYPE_H
#define LAB_2_INCORRECTBLOCKTYPE_H


#include <exception>
#include <string>

namespace Exceptions {

    class IncorrectBlockType : public std::exception {
    private:
        const std::string message;
    public:
        explicit IncorrectBlockType(const std::string &m = "Incorrect type of block!");

        char const *what() const noexcept override;
    };
}

#endif //LAB_2_INCORRECTBLOCKTYPE_H
