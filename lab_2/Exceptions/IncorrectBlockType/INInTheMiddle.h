//
// Created by Elizaveta on 10.12.2021.
//

#ifndef LAB_2_ININTHEMIDDLE_H
#define LAB_2_ININTHEMIDDLE_H


#include "IncorrectBlockType.h"

namespace Exceptions {

    class INInTheMiddle : public IncorrectBlockType {
    public:
        explicit INInTheMiddle(const std::string &m = "Type IN in the middle!");
    };
}

#endif //LAB_2_ININTHEMIDDLE_H
