//
// Created by Elizaveta on 04.12.2021.
//

#ifndef LAB_2_IMPOSSIBLETOOPENFILE_H
#define LAB_2_IMPOSSIBLETOOPENFILE_H


#include <string>
#include <exception>

namespace Exceptions {

    class ImpossibleToOpenFile : public std::exception {
    private:
        const std::string message;
        const std::string fileName;
    public:
        explicit ImpossibleToOpenFile(const std::string &fileName, const std::string &m = "Impossible to open file ");

        char const *what() const noexcept override;
    };
}

#endif //LAB_2_IMPOSSIBLETOOPENFILE_H
