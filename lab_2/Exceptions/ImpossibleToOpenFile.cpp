//
// Created by Elizaveta on 04.12.2021.
//

#include <cstring>
#include "ImpossibleToOpenFile.h"
#include "../Constants.h"

namespace Exceptions {

    ImpossibleToOpenFile::ImpossibleToOpenFile(const std::string &fileName, const std::string &m) : fileName(fileName),
                                                                                             message(m) {
    }

    char const *ImpossibleToOpenFile::what() const noexcept {
        return (message + fileName + '!').c_str();
    }
}