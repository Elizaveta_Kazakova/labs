#include <iostream>
#include "WorkFlow/WorkFlow.h"
#include "Exceptions/ParserExceptions/EqualityMoreThanOnce.h"
#include "Exceptions/ParserExceptions/ID/LetterInID.h"
#include "Exceptions/ParserExceptions/MissingCommandName.h"
#include "Exceptions/ParserExceptions/OrderList/MissingOrderOfExe.h"
#include "Exceptions/ParserExceptions/NoDescription.h"
#include "Exceptions/FactoryExceptions/CreateUnrecognisedBlock.h"
#include "Exceptions/BlockArgs/TooFewArgsForBlock.h"
#include "Exceptions/BlockArgs/TooManyArgsForBlock.h"
#include "Exceptions/ImpossibleToOpenFile.h"
#include "Exceptions/IncorrectBlockType/INInTheMiddle.h"
#include "Exceptions/IncorrectBlockType/OUTInTheMiddle.h"

int main() {
    WorkFlow::WorkFlow workflow;
    try {
        workflow.Execute("workflow.txt");
    }
    catch (Exceptions::LetterInID &ex) { // parse exceptions
        std::cerr << ex.what() << std::endl;
    }
    catch (Exceptions::MissingCommandName &ex) {
        std::cerr << ex.what() << std::endl;
    }
    catch (Exceptions::MissingOrderOfExe &ex) {
        std::cerr << ex.what() << std::endl;
    }
    catch (Exceptions::NoDescription &ex) {
        std::cerr << ex.what() << std::endl;
    }
    catch (Exceptions::TooFewArguments &ex) {
        std::cerr << ex.what() << std::endl;
    }
    catch (Exceptions::IncorrectID &ex) {
        std::cerr << ex.what() << std::endl;
    }
    catch (Exceptions::IncorrectOrderList &ex) {
        std::cerr << ex.what() << std::endl;
    }
    catch (Exceptions::EqualityMoreThanOnce &ex) {
        std::cerr << ex.what() << std::endl;
    }

    catch (Exceptions::CreateUnrecognisedBlock &ex) { // factory exceptions
        std::cerr << ex.what() << std::endl;
    }
    catch (Exceptions::FactoryBlockError &ex) {
        std::cerr << ex.what() << std::endl;
    }

    catch (Exceptions::TooFewArgsForBlock &ex) { // block exceptions
        std::cerr << ex.what() << std::endl;
    }
    catch (Exceptions::TooManyArgsForBlock &ex) {
        std::cerr << ex.what() << std::endl;
    }
    catch (Exceptions::IncorrectArgsForBlock &ex) {
        std::cerr << ex.what() << std::endl;
    }

    catch (Exceptions::ImpossibleToOpenFile &ex) {
        std::cerr << ex.what() << std::endl;
    }

    catch (Exceptions::INInTheMiddle &ex) { // blocktype exceptions
        std::cerr << ex.what() << std::endl;
    }
    catch (Exceptions::OUTInTheMiddle &ex) {
        std::cerr << ex.what() << std::endl;
    }
    catch (Exceptions::IncorrectBlockType &ex) {
        std::cerr << ex.what() << std::endl;
    }
    return 0;
}
