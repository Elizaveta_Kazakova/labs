#ifndef LAB_2_BLOCKTYPE_H
#define LAB_2_BLOCKTYPE_H

enum class BlockType {
    IN,
    OUT,
    INOUT,
};

#endif //LAB_2_BLOCKTYPE_H
