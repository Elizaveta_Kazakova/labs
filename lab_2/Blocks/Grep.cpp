#include <iostream>
#include "Grep.h"
#include "../Constants.h"
#include "../Exceptions/BlockArgs/TooManyArgsForBlock.h"
#include "../Exceptions/BlockArgs/TooFewArgsForBlock.h"

using Constants::GREP_NUM_OF_ARGS;
using Constants::WORD_INDEX_FOR_GREP;

namespace Blocks {

    Grep::Grep() : blockType(BlockType::INOUT) { }

    BlockType Grep::GetType() {
        return blockType;
    }

    std::list<std::string> Grep::Execute(const std::vector<std::string> &args, std::list<std::string> &text) {
        if (args.size() > GREP_NUM_OF_ARGS) {
            throw Exceptions::TooManyArgsForBlock("grep");
        }
        if (args.size() < GREP_NUM_OF_ARGS) {
            throw Exceptions::TooFewArgsForBlock("grep");
        }
        std::string word = args[WORD_INDEX_FOR_GREP];
        auto it = text.begin();
        while (it != text.end()) {
            if ((*it).find(word) != std::string::npos) {
                ++it;
                continue;
            }
            it = text.erase(it);
        }
        return text;
    }



}
