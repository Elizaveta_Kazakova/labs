//
// Created by Elizaveta on 13.11.2021.
//

#include <iostream>
#include "ReadFile.h"
#include "../Constants.h"
#include "../Exceptions/BlockArgs/TooManyArgsForBlock.h"
#include "../Exceptions/ImpossibleToOpenFile.h"

using Constants::READFILE_NUM_OF_ARGS;
using Constants::FILENAME_INDEX_FOR_BLOCKS;

namespace Blocks {

    ReadFile::ReadFile() : blockType(BlockType::IN) { }

    std::list<std::string> ReadFile::Execute(const std::vector<std::string> &args, std::list<std::string> &text) {
        if (args.size() != READFILE_NUM_OF_ARGS) {
            throw Exceptions::TooManyArgsForBlock("readfile");
        }
        std::ifstream fin;
        fin.open(args[FILENAME_INDEX_FOR_BLOCKS]);
        if (!fin.is_open()) {
            throw Exceptions::ImpossibleToOpenFile(args[FILENAME_INDEX_FOR_BLOCKS]);
        }
        fin >> text;
        return text;
    }

    BlockType ReadFile::GetType() {
        return blockType;
    }


    std::istream &operator>>(std::istream &in, std::list<std::string> &text) {
        while(!in.eof()) {
            std::string curStr;
            std::getline(in, curStr);
            text.push_back(curStr);
        }
        return in;
    }
}