#include <iostream>
#include "Sort.h"
#include "../Constants.h"
#include "../Exceptions/BlockArgs/TooManyArgsForBlock.h"


namespace Blocks {

    Sort::Sort() : blockType(BlockType::INOUT) { }

    BlockType Sort::GetType() {
        return blockType;
    }

    std::list<std::string> Sort::Execute(const std::vector<std::string> &args, std::list<std::string> &text) {
        if (args.size() > Constants::SORT_NUM_OF_ARGS) {
            throw Exceptions::TooManyArgsForBlock("sort");
        }
        text.sort();
        return text;
    }


}