//
// Created by Elizaveta on 25.11.2021.
//

#ifndef LAB_2_REPLACE_H
#define LAB_2_REPLACE_H

#include <list>
#include <string>
#include <vector>
#include "../Constants.h"
#include "BlockType.h"
#include "IBlock.h"

namespace Blocks {

    class Replace : public IBlock {
    private:
        BlockType blockType;
    public:

        Replace();

        std::list<std::string> Execute(const std::vector<std::string>& args, std::list<std::string>& text) override;

        BlockType GetType() override;

        ~Replace() = default;
    };
}


#endif //LAB_2_REPLACE_H
