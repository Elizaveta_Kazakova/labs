#include <iostream>
#include <algorithm>
#include "Replace.h"
#include "../Constants.h"
#include "../Exceptions/BlockArgs/TooManyArgsForBlock.h"
#include "../Exceptions/BlockArgs/TooFewArgsForBlock.h"

using Constants::REPLACE_NUM_OF_ARGS;
using Constants::WORD1_INDEX_FOR_REPLACE;
using Constants::WORD2_INDEX_FOR_REPLACE;


namespace Blocks {

    Replace::Replace() : blockType(BlockType::INOUT) { }

    BlockType Replace::GetType() {
        return blockType;
    }

    std::list<std::string> Replace::Execute(const std::vector<std::string>& args, std::list<std::string>& text) {
        if (args.size() > REPLACE_NUM_OF_ARGS) {
            throw Exceptions::TooManyArgsForBlock("replace");
        }
        if (args.size() < REPLACE_NUM_OF_ARGS) {
            throw Exceptions::TooFewArgsForBlock("replace");
        }
        const std::string word1 = args[WORD1_INDEX_FOR_REPLACE];
        const std::string word2 = args[WORD2_INDEX_FOR_REPLACE];
        for (auto &str: text) {
            std::string::size_type pos = 0;
            size_t occurrences = 0;
            while ((pos = str.find(word1, pos)) != std::string::npos) {
                ++occurrences;
                pos += word1.length();
            }
            size_t numOfRep = 0;
            while (numOfRep != occurrences) {
                str.replace(str.find(word1), word1.length(), word2);
                ++numOfRep;
            }
        }
        return text;
    }
}