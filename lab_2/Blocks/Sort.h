#ifndef LAB_2_SORT_H
#define LAB_2_SORT_H

#include <list>
#include <string>
#include <vector>
#include "BlockType.h"
#include "IBlock.h"

namespace Blocks {

    class Sort : public IBlock {
    private:
        BlockType blockType;
    public:

        Sort();

        std::list<std::string> Execute(const std::vector<std::string>& args, std::list<std::string>& text) override;

        BlockType GetType() override;

        ~Sort() = default;
    };

}

#endif //LAB_2_SORT_H
