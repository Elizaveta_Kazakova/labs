#ifndef LAB_2_READFILE_H
#define LAB_2_READFILE_H

#include <string>
#include <fstream>
#include <vector>
#include "BlockType.h"
#include "IBlock.h"

namespace Blocks {

    class ReadFile : public IBlock {
    private:
        BlockType blockType;
    public:
        ReadFile();

        std::list<std::string> Execute(const std::vector<std::string> &args, std::list<std::string> &text) override;

        BlockType GetType() override;

        ~ReadFile() = default;
    };

    std::istream &operator>> (std::istream &in, std::list<std::string> &text);
}

#endif //LAB_2_READFILE_H
