//
// Created by Elizaveta on 25.11.2021.
//

#ifndef LAB_2_GREP_H
#define LAB_2_GREP_H

#include <string>
#include <fstream>
#include <vector>
#include "BlockType.h"
#include "IBlock.h"

namespace Blocks {

    class Grep : public IBlock {
    private:
        BlockType blockType;
        //bool ContainsTheWord(std::string str, std::string word);
    public:
        Grep();

        std::list <std::string> Execute(const std::vector<std::string> &args, std::list<std::string> &text) override;

        BlockType GetType() override;

        ~Grep() = default;
    };
}


#endif //LAB_2_GREP_H
