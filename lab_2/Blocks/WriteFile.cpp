//
// Created by Elizaveta on 25.11.2021.
//

#include <string>
#include <iostream>
#include "WriteFile.h"
#include "../Constants.h"
#include "../Exceptions/BlockArgs/TooManyArgsForBlock.h"
#include "../Exceptions/BlockArgs/TooFewArgsForBlock.h"
#include "../Exceptions/ImpossibleToOpenFile.h"

using Constants::WRITEFILE_NUM_OF_ARGS;
using Constants::FILENAME_INDEX_FOR_BLOCKS;

namespace Blocks {

    WriteFile::WriteFile() : blockType(BlockType::OUT) { }

    BlockType WriteFile::GetType() {
        return blockType;
    }

    std::list<std::string> WriteFile::Execute(const std::vector<std::string> &args, std::list<std::string> &text) {
        if (args.size() > WRITEFILE_NUM_OF_ARGS) {
            throw Exceptions::TooManyArgsForBlock("writeFile");
        }
        if (args.size() < WRITEFILE_NUM_OF_ARGS) {
            throw Exceptions::TooFewArgsForBlock("writeFile");
        }
        std::ofstream fout;
        fout.open(args[FILENAME_INDEX_FOR_BLOCKS]);
        if (!fout.is_open()) {
            throw Exceptions::ImpossibleToOpenFile(args[FILENAME_INDEX_FOR_BLOCKS]);
        }
        fout << text;
        std::list <std::string> emptyText;
        return emptyText;
    }

    std::ostream &operator<<(std::ostream &out, std::list<std::string> &text) {
        for (const auto& str : text) {
            out << str << std::endl;
        }
        return out;
    }

}