//
// Created by Elizaveta on 25.11.2021.
//

#ifndef LAB_2_DUMP_H
#define LAB_2_DUMP_H

#include <string>
#include <list>
#include <vector>
#include <fstream>
#include "BlockType.h"
#include "IBlock.h"


namespace Blocks {
    class Dump : public IBlock {
    private:
        BlockType blockType;

    public:

        Dump();

        std::list<std::string> Execute(const std::vector<std::string>& args, std::list<std::string>& text) override;

        BlockType GetType() override;

        ~Dump() = default;

    };

    std::ostream &operator<<(std::ostream &out, std::list<std::string> text);
}


#endif //LAB_2_DUMP_H
