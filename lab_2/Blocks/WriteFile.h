//
// Created by Elizaveta on 25.11.2021.
//

#ifndef LAB_2_WRITEFILE_H
#define LAB_2_WRITEFILE_H

#include <vector>
#include <fstream>
#include "IBlock.h"

namespace Blocks {

    class WriteFile : public IBlock {
    private:
        BlockType blockType;

    public:

        WriteFile();

        std::list<std::string> Execute(const std::vector<std::string> &args, std::list<std::string> &text) override;

        BlockType GetType() override;

        ~WriteFile() = default;
    };

    std::ostream &operator<<(std::ostream &out, std::list<std::string> &text);
}
#endif //LAB_2_WRITEFILE_H
