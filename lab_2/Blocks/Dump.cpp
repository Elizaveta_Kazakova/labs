//
// Created by Elizaveta on 25.11.2021.
//

#include "Dump.h"
#include "../Constants.h"
#include "../Exceptions/BlockArgs/TooManyArgsForBlock.h"
#include "../Exceptions/BlockArgs/TooFewArgsForBlock.h"
#include "../Exceptions/ImpossibleToOpenFile.h"

using Constants::DUMP_NUM_OF_ARGS;
using Constants::FILENAME_INDEX_FOR_BLOCKS;

namespace Blocks {

    Dump::Dump(): blockType(BlockType::INOUT) { }

    BlockType Dump::GetType() {
        return blockType;
    }

    std::list<std::string> Dump::Execute(const std::vector<std::string>& args, std::list<std::string>& text) {
        std::ofstream fout;
        if (args.size() > DUMP_NUM_OF_ARGS) {
            throw Exceptions::TooManyArgsForBlock("dump");
        }
        if (args.size() < DUMP_NUM_OF_ARGS) {
            throw Exceptions::TooFewArgsForBlock("dump");
        }
        fout.open(args[FILENAME_INDEX_FOR_BLOCKS]);
        if (!fout.is_open()) {
            throw Exceptions::ImpossibleToOpenFile(args[FILENAME_INDEX_FOR_BLOCKS]);
        }
        fout << text;
        return text;
    }

    std::ostream &operator<<(std::ostream &out, std::list<std::string> text) {
        for (const auto& str : text) {
            out << str << std::endl;
        }
        return out;
    }
}