//
// Created by Elizaveta on 21.11.2021.
//

#ifndef LAB_2_IBLOCK_H
#define LAB_2_IBLOCK_H

#include <list>
#include "BlockType.h"

namespace Blocks {

    class IBlock {
    public:
        virtual std::list<std::string> Execute(const std::vector<std::string> &args, std::list<std::string> &text) = 0;

        virtual BlockType GetType() = 0;

        ~IBlock() = default;
    };


}

#endif //LAB_2_IBLOCK_H
