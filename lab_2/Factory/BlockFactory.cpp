#include <iostream>
#include "BlockFactory.h"
#include "../Exceptions/FactoryExceptions/CreateUnrecognisedBlock.h"
#include "../Blocks/Dump.h"
#include "../Blocks/Grep.h"
#include "../Blocks/ReadFile.h"
#include "../Blocks/Replace.h"
#include "../Blocks/Sort.h"
#include "../Blocks/WriteFile.h"

namespace WorkFlow {

    Blocks::IBlock *BlockFactory::Create(const std::string& blockName) const {
        if (blockName == "dump") {
            return new Blocks::Dump();
        }
        if (blockName == "grep") {
            return new Blocks::Grep();
        }
        if (blockName == "readfile") {
            return new Blocks::ReadFile();
        }
        if (blockName == "replace") {
            return new Blocks::Replace();
        }
        if (blockName == "dump") {
            return new Blocks::Dump();
        }
        if (blockName == "sort") {
            return new Blocks::Sort();
        }
        if (blockName == "writefile") {
            return new Blocks::WriteFile();
        }
        throw Exceptions::CreateUnrecognisedBlock(blockName);
    }

}
