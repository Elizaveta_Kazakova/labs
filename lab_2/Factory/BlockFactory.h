#ifndef LAB_2_BLOCKFACTORY_H
#define LAB_2_BLOCKFACTORY_H

#include <map>
#include <vector>

#include "../Blocks/IBlock.h"

namespace WorkFlow {

    class BlockFactory  {
    public:
        Blocks::IBlock *Create(const std::string& blockName) const;
    };
}


#endif //LAB_2_BLOCKFACTORY_H
