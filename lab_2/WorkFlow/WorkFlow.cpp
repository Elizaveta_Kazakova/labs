//
// Created by Elizaveta on 14.11.2021.
//

#include <iostream>
#include "WorkFlow.h"
#include "WorkFlowExecutor.h"
#include "../Exceptions/ImpossibleToOpenFile.h"

namespace WorkFlow {

    void WorkFlow::CheckFile(const std::string &fileName) {
        if (!workFlowFile.is_open()) {
            throw Exceptions::ImpossibleToOpenFile(fileName.c_str());
        }
    }

    void WorkFlow::Execute(const std::string &fileName) {
        workFlowFile.open(fileName);
        CheckFile(fileName);
        WorkFlowExecutor::ExecuteWorkFlow(workFlowFile);
    }
}
