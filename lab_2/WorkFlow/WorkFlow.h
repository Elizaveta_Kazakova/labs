//
// Created by Elizaveta on 14.11.2021.
//

#ifndef LAB_2_WORKFLOW_H
#define LAB_2_WORKFLOW_H

#include <fstream>
#include <list>
#include <vector>

namespace WorkFlow {

class WorkFlow {

 private:
        std::ifstream workFlowFile;
        void CheckFile(const std::string &fileName);
    public:
        void Execute(const std::string &fileName);
};
}


#endif //LAB_2_WORKFLOW_H
