#ifndef LAB_2_WORKFLOWPARSER_H
#define LAB_2_WORKFLOWPARSER_H

#include <list>
#include <string>
#include <vector>
#include <iostream>
#include <map>

class WorkFlowParser {
private:
    std::map<size_t , std::pair <std::string, std::vector<std::string>>> commandList;
    std::list<size_t> exeOrder;
    void ParseInput(std::istream &in);
    void HandleWhiteSpace(char el, std::string &nameStr, std::string &commandName, std::string &argStr,
                          std::vector<std::string> &args);
    void HandleEq(bool &rightSide);
    void HandleLeftSide(char el, bool &rightSide, long long &id);
    void HandleRightSide(char el, std::string &commandName, std::string &nameStr,
                         std::string &argStr, std::vector<std::string> &args);
    void HandleNotEmptyArgs(std::string &nameStr, std::string &commandName,
                            std::string &argStr, std::vector<std::string> &args);
    bool ParseStr(const std::string& str, long long &id, std::string &commandName,
                  std::vector <std::string> &args, size_t line);
    static void AddToNumber(char el, long long &id);
    void CheckId(long long &id);
    void ParseOrder(std::string &order);
public:
    std::map<size_t , std::pair <std::string, std::vector<std::string>>> GetBlocks(std::istream &in);
    std::list <size_t> GetOrderOfExe(std::istream &in);
};


#endif //LAB_2_WORKFLOWPARSER_H
