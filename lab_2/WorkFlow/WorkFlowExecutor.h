#ifndef LAB_2_WORKFLOWEXECUTOR_H
#define LAB_2_WORKFLOWEXECUTOR_H

#include <string>
#include <map>
#include <vector>
#include "../Blocks/IBlock.h"
#include "../Factory/BlockFactory.h"

namespace WorkFlow {
    class WorkFlowExecutor {
    private:
        static void FillBlockObjects(std::map<size_t , std::pair <std::string, std::vector<std::string>>> &blocks,
                              std::map<std::string, Blocks::IBlock *> &blockObjects, BlockFactory factory);
        static void DeleteBlockObjects(std::map<std::string, Blocks::IBlock *> &blockObjects);
    public:
        static void ExecuteWorkFlow(std::istream &in);
    };
}

#endif //LAB_2_WORKFLOWEXECUTOR_H
