#include "WorkFlowExecutor.h"
#include "WorkFlowParser.h"
#include "../Exceptions/IncorrectBlockType/INInTheMiddle.h"
#include "../Exceptions/IncorrectBlockType/OUTInTheMiddle.h"
#include "../Constants.h"

namespace WorkFlow {
    void WorkFlowExecutor::ExecuteWorkFlow(std::istream &in) {
        WorkFlowParser parser;
        auto blocks = parser.GetBlocks(in);
        std::list<std::string> text;
        std::map<std::string, Blocks::IBlock *> blockObjects;
        WorkFlow::BlockFactory factory;
        FillBlockObjects(blocks, blockObjects, factory);
        auto orderOfExe = parser.GetOrderOfExe(in);
        size_t index = 0;
        for (size_t id : orderOfExe) {
            ++index;
            if (blockObjects[blocks[id].first]->GetType() == BlockType::IN && index != Constants::INDEX_FOR_IN_TYPE) {
                throw Exceptions::INInTheMiddle();
            }
            if (blockObjects[blocks[id].first]->GetType() == BlockType::OUT && index != orderOfExe.size()) {
                throw Exceptions::OUTInTheMiddle();
            }
            text = blockObjects[blocks[id].first]->Execute(blocks[id].second, text);
        }
        DeleteBlockObjects(blockObjects);
    }

    void WorkFlowExecutor::FillBlockObjects(std::map<size_t, std::pair<std::string, std::vector<std::string>>> &blocks,
                                std::map<std::string, Blocks::IBlock *> &blockObjects, BlockFactory factory) {
        for (const auto& block : blocks) {
            std::string blockName = block.second.first;
            if (blockObjects.find(blockName) == blockObjects.end()) {
                Blocks::IBlock *blockObject = factory.Create(blockName);
                blockObjects.insert(std::make_pair(blockName, blockObject));
            }
        }
    }

    void WorkFlowExecutor::DeleteBlockObjects(std::map<std::string, Blocks::IBlock *> &blockObjects) {
        for (const auto& blockObject : blockObjects) {
            delete(blockObject.second);
        }
    }

    \

}
