#include "WorkFlowParser.h"
#include "../Constants.h"
#include "../Exceptions/ParserExceptions/ID/LetterInID.h"
#include "../Exceptions/ParserExceptions/EqualityMoreThanOnce.h"
#include "../Exceptions/ParserExceptions/MissingCommandName.h"
#include "../Exceptions/ParserExceptions/NoDescription.h"
#include "../Exceptions/ParserExceptions/OrderList/IncorrectOrderList.h"
#include "../Exceptions/ParserExceptions/OrderList/MissingOrderOfExe.h"
#include "../Exceptions/ParserExceptions/ID/MatchingID.h"
#include <cctype>


std::map<size_t , std::pair <std::string, std::vector<std::string>>> WorkFlowParser::GetBlocks(std::istream &in) {
    if(commandList.empty()) {
        ParseInput(in);
    }
    return commandList;
}

std::list<size_t> WorkFlowParser::GetOrderOfExe(std::istream &in) {
    if (exeOrder.empty()) {
        ParseInput(in);
    }
    return exeOrder;
}

void WorkFlowParser::AddToNumber(char el, long long &id) {
    id = id * 10 + (el - '0');
}

bool WorkFlowParser::ParseStr(const std::string& str, long long &id, std::string &commandName,
                              std::vector <std::string> &args, size_t line) {
    bool rightSide = false;
    std::string nameStr;
    std::string argStr;
    if (str == Constants::END_DESCRIPTION) {
        return false;
    }
    for (auto el : str) {
        if (el == ' ') {
            HandleWhiteSpace(el, nameStr, commandName, argStr, args);
            continue;
        }
        if (el == '=') {
            HandleEq(rightSide);
            continue;
        }
        if (!rightSide) {
            HandleLeftSide(el, rightSide, id);
            continue;
        }
        if (rightSide) {
            HandleRightSide(el, commandName, nameStr, argStr, args);
        }
    }
    HandleNotEmptyArgs(nameStr, commandName, argStr, args);
    if (id == Constants::NOT_AN_ID) {
        throw Exceptions::IncorrectID();
    }
    if (commandName.empty()) {
        throw Exceptions::MissingCommandName(line);
    }
    return true;
}


void WorkFlowParser::CheckId(long long &id) {
    if (commandList.find(id) != commandList.end()) {
        throw Exceptions::MatchingID();
    }
}

void WorkFlowParser::ParseInput(std::istream &in) {
    std::string commandName;
    std::vector <std::string> args;
    long long id = Constants::NOT_AN_ID;
    std::string curStr;
    size_t line = Constants::START_LINE;
    while (curStr != Constants::START_DESCRIPTION && !in.eof()) {
        getline(in, curStr);
        line++;
    }
    if (curStr != Constants::START_DESCRIPTION) {
        throw Exceptions::NoDescription();
    }
    while (!in.eof()) {
        getline(in, curStr);
        if (ParseStr(curStr, id, commandName, args, line)) {
            CheckId(id);
            commandList.insert(std::pair<size_t, std::pair<std::string,
                    std::vector<std::string>>>(id, std::make_pair(commandName, args)));
            id = Constants::NOT_AN_ID;
            curStr.clear();
            args.clear();
            commandName.clear();
            ++line;
            continue;
        }

        if (!in.eof()) {
            getline(in, curStr);
        }
        ParseOrder(curStr);
    }
}

void WorkFlowParser::ParseOrder(std::string &order) {
    long long id = Constants::DEFAULT_ID;
    for (auto el : order) {
        if (el == '-') {
            exeOrder.push_back(id);
            id = Constants::DEFAULT_ID;
            continue;
        }
        if (el == '>' || el == ' ') continue;
        if(!isdigit(el)) {
            throw Exceptions::IncorrectOrderList();
        }
        AddToNumber(el, id);
    }
    exeOrder.push_back(id);
    if (exeOrder.empty()) {
        throw Exceptions::MissingOrderOfExe();
    }
}

void WorkFlowParser::HandleWhiteSpace(char el, std::string &nameStr, std::string &commandName, std::string &argStr,
                                      std::vector<std::string> &args) {
    if (!nameStr.empty()) {
        commandName = nameStr;
        nameStr.clear();
        return;
    }
    if (!argStr.empty()) {
        args.push_back(argStr);
        argStr.clear();
        return;
    }
}

void WorkFlowParser::HandleLeftSide(char el, bool &rightSide, long long &id) {
    if (isdigit(el)) {
        if (id == Constants::NOT_AN_ID) {
            id = Constants::DEFAULT_ID;
        }
        AddToNumber(el, id);
        return;
    }
    if (isalpha(el)) {
        throw Exceptions::LetterInID();
    }
    if (!isdigit(el)) {
        throw Exceptions::IncorrectID();
    }
}

void WorkFlowParser::HandleEq(bool &rightSide) {
    if (rightSide) {
        throw Exceptions::EqualityMoreThanOnce();
    }
    rightSide = true;
}

void WorkFlowParser::HandleRightSide(char el, std::string &commandName,  std::string &nameStr,
                                     std::string &argStr, std::vector<std::string> &args) {
    if (!commandName.empty()) {
        argStr.push_back(el);
        return;
    }
    if (args.empty()) {
        nameStr.push_back(el);
        return;
    }
}

void WorkFlowParser::HandleNotEmptyArgs(std::string &nameStr, std::string &commandName, std::string &argStr,
                                        std::vector<std::string> &args) {
    if (!argStr.empty()) {
        args.push_back(argStr);
        argStr.clear();
    }
    if (!nameStr.empty()) {
        commandName = nameStr;
        nameStr.clear();
    }
}

