#ifndef LAB_2_CONSTANTS_H
#define LAB_2_CONSTANTS_H

#include <string>

namespace Constants {
    const std::string START_DESCRIPTION{"desc"};
    const std::string END_DESCRIPTION{"csed"};
    const size_t DUMP_NUM_OF_ARGS{1};
    const size_t GREP_NUM_OF_ARGS{1};
    const size_t SORT_NUM_OF_ARGS{0};
    const size_t REPLACE_NUM_OF_ARGS{2};
    const size_t READFILE_NUM_OF_ARGS{1};
    const size_t WRITEFILE_NUM_OF_ARGS{1};
    const size_t FILENAME_INDEX_FOR_BLOCKS{0};
    const size_t WORD_INDEX_FOR_GREP{0};
    const size_t WORD1_INDEX_FOR_REPLACE{0};
    const size_t WORD2_INDEX_FOR_REPLACE{1};
    const long long NOT_AN_ID{-1};
    const size_t DEFAULT_ID{0};
    const size_t START_LINE{1};
    const size_t INDEX_FOR_IN_TYPE{1};
}

#endif //LAB_2_CONSTANTS_H
