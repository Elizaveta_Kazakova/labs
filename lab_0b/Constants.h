#ifndef LAB_0B_CONSTANTS_H
#define LAB_0B_CONSTANTS_H

#include <string>

namespace Constants {
    const int numOfInputArg{3};
    const int successfulExe{0};
    const int unsuccessfulExe{1};
    const int terminalSymbolSize{1};
    const int argOfInputFile{1};
    const int argOfOutputFile{2};
    const int startPosInParser{0};
    const int startPosInStr{0};
    const std::wstring invalidNumOfArg{L"Invalid number of arguments! Please, enter txt and csv file."};
    const std::wstring wrongInput{L"Wrong name of input file!"};
    const std::wstring impossibleToOpen{L"Impossible to open "};
    const std::wstring inputType{L".txt"};
    const std::wstring outputType{L".csv"};
    const std::wstring wrongOutput{L"Wrong name of output file!"};
    const wchar_t lowFirstRusLetter {L'а'};
    const wchar_t lowLastRusLetter {L'я'};
    const wchar_t upFirstRusLetter {L'А'};
    const wchar_t upLastRusLetter {L'Я'};
    const wchar_t wstrComma{L';'};
    const std::wstring word{L"Word"};
    const std::wstring frequency{L"Frequency"};
    const std::wstring percentFrequency{L"Percent frequency"};
}


#endif //LAB_0B_CONSTANTS_H
