#include <iostream>
#include "ErrorsDisplayer.h"
#include "Constants.h"

using namespace Constants;

ErrorsDisplayer::ErrorsDisplayer(std::wstring &fileName, const std::wstring &fileType)
    : fileName(fileName), fileType(fileType) {
}

void ErrorsDisplayer::CheckOpen(std::wfstream &file) {
    if (!file.is_open()) {
        std::wcout << impossibleToOpen << fileName << std::endl;
        exit(0);
    }
}
void ErrorsDisplayer::CheckTypeFile() {
    if(fileName.find(fileType) == std::wstring::npos) {
        if (fileType == inputType) {
            std::wcout << wrongInput << std::endl;
            exit(0);
        }
        if (fileType == outputType) {
            std::wcout << wrongOutput << std::endl;
            exit(0);
        }
    }
}
