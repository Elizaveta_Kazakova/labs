#include <iostream>
#include "WideStrFileWorker.h"
#include "WordParser.h"
#include "CSVCreator.h"
#include "StrConverter.h"
#include "Constants.h"

using namespace FileParser;
using namespace Constants;

int main(int argc, char **argv) {
    setlocale(LC_ALL, "");
    if (argc != numOfInputArg) {
        std::wcout << invalidNumOfArg << std::endl;
        return successfulExe;
    }
    StrConverter inputFilename{static_cast<std::string>(argv[argOfInputFile])};
    std::wstring wstrInputFilename = inputFilename.ConvertStrToWstr();
    std::string strInputFilename = inputFilename.GetStr();
    auto input = new WideStrFileWorker{strInputFilename, ErrorsDisplayer{wstrInputFilename, inputType}};
    input->TypeErr();
    input->OpenFile();
    auto wordParser = new WordParser;
    wordParser->ParseFile(input);
    input->CloseFile();
    delete input;
    StrConverter outputFilename{static_cast<std::string>(argv[argOfOutputFile])};
    std::wstring wstrOutputFilename = outputFilename.ConvertStrToWstr();
    std::string strOutputFilename = outputFilename.GetStr();
    auto output = new WideStrFileWorker {strOutputFilename, ErrorsDisplayer{wstrOutputFilename, outputType}};
    output->TypeErr();
    output->OpenFile();
    auto csvCreator = new CSVCreator {wordParser->GetNumOfWords(), output->GetFile()};
    csvCreator->FillTableWithParser(wordParser);
    csvCreator->WriteCSVFile();
    output->CloseFile();
    delete wordParser;
    delete output;
    delete csvCreator;
    return successfulExe;
}
