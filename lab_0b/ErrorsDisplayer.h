//
// Created by Елизавета on 02.10.2021.
//

#ifndef LAB_0B_ERRORSDISPLAYER_H
#define LAB_0B_ERRORSDISPLAYER_H

#include <string>
#include <fstream>

class ErrorsDisplayer {

private:
    const std::wstring fileType;
    std::wstring fileName;

public:
    ErrorsDisplayer(std::wstring &fileName, const std::wstring &fileType);
    void CheckOpen(std::wfstream &file);
    void CheckTypeFile();

};


#endif //LAB_0B_ERRORSDISPLAYER_H
