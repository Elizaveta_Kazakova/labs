#ifndef LAB_0B_WIDESTRFILEWORKER_H
#define LAB_0B_WIDESTRFILEWORKER_H

#include <string>
#include <fstream>
#include "ErrorsDisplayer.h"

using std::wstring;

namespace FileParser {

    class WideStrFileWorker {
    private:
        std::string fileName;
        std::wfstream file;
        ErrorsDisplayer errDisp;

    public:
        WideStrFileWorker(std::string &fileName, ErrorsDisplayer displayer);
        void TypeErr();
        void OpenFile();
        int GetNextStr(wstring &newStr);
        void CloseFile();
        std::wfstream &GetFile();
    };
}

#endif //LAB_0B_WIDESTRFILEWORKER_H
