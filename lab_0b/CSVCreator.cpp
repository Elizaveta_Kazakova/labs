#include "CSVCreator.h"
#include <algorithm>
#include "Constants.h"

using namespace Constants;

namespace FileParser {

     bool CSVCreator::comp(const std::pair<wstring, int>& a, const std::pair<wstring, int>& b) {
        return a.second > b.second;
    }

    CSVCreator::CSVCreator(int numOfWords, std::wfstream &fout) : fout(fout) {
        this->numOfWords = numOfWords;
    }

    void CSVCreator::FillTableWithParser(WordParser *wordParser) {
        int numOfDifWords = wordParser->GetNumOfUniq();
        for (int i = startPosInParser; i < numOfDifWords; ++i) {
            wstring nextWord = wordParser->GetWord(i);
            int freq = wordParser->GetFreq(i);
            AddToCSVTable(nextWord, freq);
        }
     }

    void CSVCreator::AddToCSVTable(const wstring &wstr, int freq) {
        std::pair<wstring, int> tableValue;
        tableValue = std::make_pair(wstr, freq);
        tableWords.push_back(tableValue);
        sort(tableWords.begin(), tableWords.end(), comp);
    }

    void CSVCreator::WriteCSVFile() {
        fout << word << wstrComma << frequency << wstrComma << percentFrequency << std::endl;
        for (auto & tableWord : tableWords) {
            double percentFreq = (static_cast<double>(tableWord.second) / numOfWords) * 100;
            fout << tableWord.first << wstrComma << tableWord.second << wstrComma << percentFreq << std::endl;
        }
    }
}