#ifndef LAB_0B_STRCONVERTER_H
#define LAB_0B_STRCONVERTER_H

#include <string>

class StrConverter {

private:
    std::wstring wstr;
    std::string str;
public:
    explicit StrConverter(std::wstring wstr);
    explicit StrConverter(std::string str);
    std::wstring ConvertStrToWstr();
    std::string ConvertWstrToStr();
    std::wstring  GetWstr() const;
    std::string  GetStr() const;
};


#endif //LAB_0B_STRCONVERTER_H
