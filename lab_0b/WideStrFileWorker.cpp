#include "WideStrFileWorker.h"
#include <codecvt>
#include "Constants.h"

using namespace Constants;

namespace FileParser {

    WideStrFileWorker::WideStrFileWorker(std::string &fileName, ErrorsDisplayer displayer)
            : errDisp(std::move(displayer)) {
        this->fileName = fileName;
    }

    void WideStrFileWorker::TypeErr() {
        errDisp.CheckTypeFile();
    }

    void WideStrFileWorker::OpenFile() {
        file.open(fileName);
        file.imbue(std::locale(file.getloc(), new std::codecvt_utf8_utf16<wchar_t>));
        errDisp.CheckOpen(file);
    }

    int WideStrFileWorker::GetNextStr(wstring &newStr) {
        if (!file.eof()) {
            std::getline(file, newStr);
            return unsuccessfulExe;
        }
        return successfulExe;
    }

    void WideStrFileWorker::CloseFile() {
        file.close();
    }

    std::wfstream &WideStrFileWorker::GetFile() {
        return file;
    }
}