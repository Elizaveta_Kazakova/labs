#ifndef LAB_0B_WORDPARSER_H
#define LAB_0B_WORDPARSER_H

#include <string>
#include <map>
#include "WideStrFileWorker.h"

using std::map;
using std::wstring;

namespace FileParser {

    class WordParser {
    private:
        map<wstring, int> wordFreq;
        int numOfWords;

        static int IsRus(int c);
        static int IsValid(int c);

    public:
        WordParser();
        void ParseStr(const wstring &str);
        void ParseFile(WideStrFileWorker *fileWorker);
        int GetNumOfUniq() const;
        int GetFreq(int shift) const;
        wstring GetWord(int shift) const;
        int GetNumOfWords() const;
    };
}
#endif //LAB_0B_WORDPARSER_H
