#include "WordParser.h"
#include <string>
#include <cctype>
#include <iostream>
#include "Constants.h"

using namespace Constants;
using std::wstring;

namespace FileParser {
     WordParser::WordParser() {
        numOfWords = 0;
    }

    int WordParser::IsRus(int sym) {
        return sym >= upFirstRusLetter && sym <= lowLastRusLetter;
    }

    int WordParser::IsValid(int sym) {
        return isdigit(sym) || isalpha(sym) || IsRus(sym);
    }

    void WordParser::ParseFile(WideStrFileWorker *fileWorker) {
        wstring wstr;
        while (fileWorker->GetNextStr(wstr)) {
            ParseStr(wstr);
        }
    }

    void WordParser::ParseStr(const wstring &str) {
        wstring word;
        int curLen = 0;
        for (wchar_t i : str) {
            curLen++;
            if (IsValid(i)) {
                word.push_back(i);
            }
            if ((!IsValid(i) || curLen == str.length()) && !word.empty()) {
                numOfWords++;
                wordFreq[word]++;
                word.clear();
            }
        }
    }

    int WordParser::GetNumOfUniq() const{
        return wordFreq.size();
    }

    int WordParser::GetFreq(int shift) const{
        auto it = wordFreq.begin();
        advance(it, shift);
        return it->second;
    }

    wstring WordParser::GetWord(int shift) const{
        auto it = wordFreq.begin();
        advance(it, shift);
        return it->first;
    }

    int WordParser::GetNumOfWords() const {
        return numOfWords;
    }
}