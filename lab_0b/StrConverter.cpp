#include "StrConverter.h"
#include <iostream>
#include "Constants.h"

using namespace Constants;

StrConverter::StrConverter(std::string str) {
    this->str = str;
}

StrConverter::StrConverter(std::wstring wstr) {
    this->wstr = wstr;
}

std::wstring StrConverter::ConvertStrToWstr() {
    const size_t cSize = str.size() + terminalSymbolSize;
    std::wstring convertedStr;
    convertedStr.resize(cSize);

    size_t cSize1;
    mbstowcs_s(&cSize1, static_cast<wchar_t*>(&convertedStr[startPosInStr]), cSize, str.c_str(), cSize);
    convertedStr.pop_back();

    return convertedStr;
}

std::string StrConverter::ConvertWstrToStr() {
    std::string convertedWstr;
    size_t maxSize = wstr.size() + terminalSymbolSize;
    convertedWstr.resize(maxSize);
    wcstombs(static_cast<char *>(&convertedWstr[startPosInStr]), static_cast<wchar_t *>(&wstr[startPosInStr]), maxSize);
    return convertedWstr;
}

std::wstring StrConverter::GetWstr() const {
    return wstr;
}
std::string  StrConverter::GetStr() const {
    return str;
}

