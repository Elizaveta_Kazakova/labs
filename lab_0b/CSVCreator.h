#ifndef LAB_0B_CSVCREATOR_H
#define LAB_0B_CSVCREATOR_H

#include <string>
#include <vector>
#include <fstream>
#include "WordParser.h"

using std::wstring;
using std::vector;

namespace FileParser {

    class CSVCreator {
    private:
        vector<std::pair<wstring, int>> tableWords;
        int numOfWords;
        std::wfstream &fout;

        static bool comp(const std::pair<wstring, int>& a, const std::pair<wstring, int>& b);

    public:
        CSVCreator(int numOfWords, std::wfstream &fout);
        void AddToCSVTable(const wstring &wstr, int freq);
        void FillTableWithParser(WordParser *wordParser);
        void WriteCSVFile();
    };
}
#endif //LAB_0B_CSVCREATOR_H
