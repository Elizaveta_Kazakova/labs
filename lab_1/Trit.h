#ifndef LAB_1_TRIT_H
#define LAB_1_TRIT_H

#include <iostream>

namespace TritSetSpace {

    enum class Trit {
        Unknown = 0, // 0 // 00
        False = 1,   // 1 // 01
        True = 2,    // 2 // 10
    };

    Trit operator~(Trit t);

    Trit operator&(Trit firstArg, Trit secondArg);

    Trit operator|(Trit firstArg, Trit secondArg);

    std::ostream &operator<<(std::ostream &out, Trit t);

}
#endif //LAB_1_TRIT_H
