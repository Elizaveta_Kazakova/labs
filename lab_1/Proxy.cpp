#include "Proxy.h"
#include "Constants.h"

using Constants::TRIT_BIT_SIZE;
using Constants::ULL_BIT_SIZE;
using Constants::FIRST_BIT;
using Constants::ZERO_BIT;
using Constants::BIT_SIZE;
using Constants::NOT_A_PLACE;

typedef unsigned long long ull;

namespace TritSetSpace {

    Proxy::Proxy(Tritset &set, size_t num, Trit curTrit) : tritset(set), tritNum(num), trit(curTrit) {
    }

    void Proxy::SetBit(ull &bits, uchar bit, ushort place) {
        ull movingOneBit = static_cast<ull>(FIRST_BIT) << place;
        if (bit) {
            bits |= movingOneBit;
        } else {
            bits &= ~(movingOneBit);
        }
    }

    void Proxy::SetTrit(Trit val) {
        ull offset = (tritNum * TRIT_BIT_SIZE + TRIT_BIT_SIZE) / ULL_BIT_SIZE; // index of current ull
        ull effset = (tritNum * TRIT_BIT_SIZE + TRIT_BIT_SIZE) % ULL_BIT_SIZE; // bit index in current ull
        ushort bitPlace = (ULL_BIT_SIZE - effset) % ULL_BIT_SIZE;

        FixVectorSize(offset);

        uchar valFirstBit = Tritset::GetBit(static_cast<ull>(val), FIRST_BIT);
        uchar valZeroBit = Tritset::GetBit(static_cast<ull>(val), ZERO_BIT);

        SetBit(tritset.values[offset], valFirstBit, bitPlace + BIT_SIZE);
        SetBit(tritset.values[offset], valZeroBit, bitPlace);

    }

    void Proxy::SetTrits(Trit val, size_t numOfTrits) {
        size_t lastTrit = tritNum + numOfTrits;
        while (tritNum < lastTrit) {
            SetTrit(val);
            ++tritNum;
        }
    }

    void Proxy::FixPlaceLastNotUnknown(Trit newVal) {
        if (newVal != Trit::Unknown &&
            (tritset.placeLastNotUnknown == NOT_A_PLACE || tritset.placeLastNotUnknown < tritNum)) {
            tritset.placeLastNotUnknown = tritNum;
        }
        if (newVal == Trit::Unknown && tritset.placeLastNotUnknown == tritNum) {
            tritset.placeLastNotUnknown = tritset.SearchLastNotUnknow();
        }
    }

    void Proxy::FixVectorSize(ull curIndex) {
        if (curIndex > tritset.values.size()) {
            tritset.values.resize(curIndex + 1, static_cast<ull>(Trit::Unknown));
        }
    }

    void Proxy::operator=(Trit value) {
        if (trit == Trit::Unknown && value == Trit::Unknown) {
            return;
        }
        SetTrit(value);
        tritset.placeOfLastSetTrit = tritNum;
        FixPlaceLastNotUnknown(value);
        tritset.FixSize();
    }

    Proxy &Proxy::operator=(Proxy anotherProxy) {
        (*this) = Trit(anotherProxy);
        return (*this);
    }

    Proxy::operator Trit() const {
        return trit;
    }
}
