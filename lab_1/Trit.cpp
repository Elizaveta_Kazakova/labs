#include "Trit.h"

namespace TritSetSpace {

    Trit operator&(Trit firstArg, Trit secondArg) {
        if (firstArg == Trit::False || secondArg == Trit::False) {
            return Trit::False;
        }
        if (firstArg == Trit::Unknown || secondArg == Trit::Unknown) {
            return Trit::Unknown;
        }
        return Trit::True;
    }

    Trit operator|(Trit firstArg, Trit secondArg) {
        if (firstArg == Trit::True || secondArg == Trit::True) {
            return Trit::True;
        }
        if (firstArg == Trit::Unknown || secondArg == Trit::Unknown) {
            return Trit::Unknown;
        }
        return Trit::False;
    }

    Trit operator~(Trit t) {
        if (t == Trit::False) return Trit::True;
        if (t == Trit::True) return Trit::False;
        return Trit::Unknown;
    }

    std::ostream &operator<<(std::ostream &out, Trit t) {
        if (t == Trit::True) {
            out << "True";
        }
        if (t == Trit::False) {
            out << "False";
        }
        if (t == Trit::Unknown) {
            out << "Unknown";
        }
        return out;
    }

}