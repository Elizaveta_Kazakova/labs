#ifndef LAB_1_CONSTANTS_H
#define LAB_1_CONSTANTS_H

#include <limits>

typedef unsigned long long ull;

namespace Constants {
    const char TRIT_BIT_SIZE{2};
    const char BINARY_BASE{2};
    const char BIT_SIZE{1};
    const char BYTE_SIZE{8};
    const short ULL_BIT_SIZE{BYTE_SIZE * sizeof(ull)};
    const short ZERO_BIT{0};
    const short FIRST_BIT{1};
    const ull MAX_ULL{std::numeric_limits<ull>::max()};
    const ull NOT_A_PLACE{MAX_ULL};
    const char NUM_OF_EL_IN_ENUM{3};
    const size_t ZERO_EL{0};
    const size_t FIRST_EL{1};
    const size_t SECOND_EL{2};
    const size_t NULL_LENGTH{0};
}

#endif //LAB_1_CONSTANTS_H
