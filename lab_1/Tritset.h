#ifndef LAB_1_TRITSET_H
#define LAB_1_TRITSET_H

#include "Trit.h"
#include <vector>
#include <unordered_map>

typedef unsigned long long ull;
typedef unsigned short ushort;
typedef unsigned char uchar;

namespace TritSetSpace {
    class Proxy;
    class Tritset {
    private:
        std::vector<ull> values;
        size_t numOfTrits;

        static size_t GetNumOfUll(size_t curNumOfTrits);

        static uchar GetBit(ull val, ushort place);

        Trit GetTrit(size_t tritNum) const;

        size_t placeOfLastSetTrit;
        size_t placeLastNotUnknown;

        void FixSize();

        size_t SearchLastNotUnknow();

        void FixPlaceLastNotUnknown();

    public:
        explicit Tritset(size_t numOfTrits);

        size_t Length() const;

        void Trim(size_t lastIndex);

        struct TritHasher {
            size_t operator()(const Trit &t) const;
        };

        std::unordered_map<Trit, int, TritHasher> Cardinality();

        Proxy operator[](size_t tritNum);

        Tritset operator~();

        Tritset operator&(Tritset set);

        Tritset operator|(Tritset set);

        //Tritset operator=(Tritset set);

        size_t Capacity() const;

        void Shrink();

        size_t Cardinality(Trit value);

        friend Proxy;
    };
}


#endif //LAB_1_TRITSET_H
