#include "gtest/gtest.h"
#include "Tritset.h"
#include "Constants.h"
#include "Proxy.h"
#include <unordered_map>

using Constants::TRIT_BIT_SIZE;
using Constants::ULL_BIT_SIZE;
using Constants::NUM_OF_EL_IN_ENUM;
using Constants::ZERO_EL;
using Constants::FIRST_EL;
using Constants::SECOND_EL;

using namespace TritSetSpace;

TEST(MemoryTests, ConstructorAllocationTest)
{
    const size_t MAX_SIZE = 1000;
    for (ull numOfTrits = 0; numOfTrits <= MAX_SIZE; ++numOfTrits)
    {
        Tritset set{numOfTrits};
        size_t allocLength = set.Capacity();
        EXPECT_GE(allocLength, numOfTrits * TRIT_BIT_SIZE / ULL_BIT_SIZE);
    }
}

TEST(MemoryTests, TritAllocationTest) {
    const size_t SIZE = 10;
    Tritset set{SIZE};
    size_t allocLength = set.Capacity();
    const size_t NOT_ALLOC_PLACE_1 = 1000;
    set[NOT_ALLOC_PLACE_1] = Trit::Unknown;
    EXPECT_EQ(allocLength, set.Capacity());
    set[NOT_ALLOC_PLACE_1] = Trit::True;
    EXPECT_LT(allocLength, set.Capacity());
    const size_t NOT_ALLOC_PLACE_2 = 2000000;
    if (set[NOT_ALLOC_PLACE_2] == Trit::True) {
        EXPECT_EQ(allocLength, set.Capacity());
    }
}


TEST(MemoryTests, LogicalOperationANDTest) {
    const size_t SMALLER_SIZE = 1000;
    const size_t BIGGER_SIZE = 2000;
    Tritset setA{SMALLER_SIZE};
    Tritset setB{BIGGER_SIZE};
    Tritset setC = setA & setB;
    EXPECT_EQ(setC.Capacity(), setB.Capacity());
    Tritset setD{BIGGER_SIZE};
    Tritset setE{SMALLER_SIZE};
    Tritset setF = setD & setE;
    EXPECT_EQ(setF.Capacity(), setD.Capacity());
}

TEST(MemoryTests, LogicalOperationORTest) {
    const size_t SMALLER_SIZE = 1000;
    const size_t BIGGER_SIZE = 2000;
    Tritset setA{SMALLER_SIZE};
    Tritset setB{BIGGER_SIZE};
    Tritset setC = setA | setB;
    EXPECT_EQ(setC.Capacity(), setB.Capacity());
    Tritset setD{BIGGER_SIZE};
    Tritset setE{SMALLER_SIZE};
    Tritset setF = setD | setE;
    EXPECT_EQ(setF.Capacity(), setD.Capacity());
}

TEST(MemoryTests, LogicalOperationNOTTest) {
    const size_t SET_SIZE = 1000;
    Tritset setA{SET_SIZE};
    Tritset setB = ~setA;
    EXPECT_EQ(setA.Capacity(), setB.Capacity());
}


TEST(TestsOfTritsetOperators, TritSquareBrackets_1) {
    Tritset set{10};
    set[0] = Trit::True;
    set[1] = Trit::Unknown;
    set[2] = Trit::False;
    set[3] = Trit::Unknown;
    set[4] = Trit::True;
    set[5] = Trit::False;
    set[6] = Trit::True;
    set[7] = Trit::Unknown;
    set[8] = Trit::False;
    set[9] = Trit::Unknown;
    EXPECT_TRUE(set[0] == Trit::True);
    EXPECT_TRUE(set[1] == Trit::Unknown);
    EXPECT_TRUE(set[2] == Trit::False);
    EXPECT_TRUE(set[3] == Trit::Unknown);
    EXPECT_TRUE(set[4] == Trit::True);
    EXPECT_TRUE(set[5] == Trit::False);
    EXPECT_TRUE(set[6] == Trit::True);
    EXPECT_TRUE(set[7] == Trit::Unknown);
    EXPECT_TRUE(set[8] == Trit::False);
    EXPECT_TRUE(set[9] == Trit::Unknown);
}


TEST(TestsOfTritsetOperators, TritSquareBrackets_2) {
    const size_t SIZE = 100;
    Tritset set{SIZE};
    for (size_t numOfTrit = 0; numOfTrit < SIZE; ++numOfTrit) {
        if (numOfTrit % NUM_OF_EL_IN_ENUM == ZERO_EL) {
            set[numOfTrit] = Trit::Unknown;
        }
        if (numOfTrit % NUM_OF_EL_IN_ENUM == FIRST_EL) {
            set[numOfTrit] = Trit::True;
        }
        if (numOfTrit % NUM_OF_EL_IN_ENUM == SECOND_EL) {
            set[numOfTrit] = Trit::False;
        }
    }
    for (size_t numOfTrit = 0; numOfTrit < SIZE; ++numOfTrit) {
        if (numOfTrit % NUM_OF_EL_IN_ENUM == ZERO_EL) {
            EXPECT_TRUE(set[numOfTrit] == Trit::Unknown);
        }
        if (numOfTrit % NUM_OF_EL_IN_ENUM == FIRST_EL) {
            EXPECT_TRUE(set[numOfTrit] == Trit::True);
        }
        if (numOfTrit % NUM_OF_EL_IN_ENUM == SECOND_EL) {
            EXPECT_TRUE(set[numOfTrit] == Trit::False);
        }
    }
}

TEST(TestsOfTritsetOperators, TritSquareBrackets_3) {
    const size_t SIZE = 100;
    Tritset set{SIZE};
    for (size_t numOfTrit = 0; numOfTrit < (SIZE / 2); ++numOfTrit) {
            set[numOfTrit] = Trit::True;
    }
    for (size_t numOfTrit = (SIZE / 2); numOfTrit < SIZE; ++numOfTrit) {
        set[numOfTrit] = set[numOfTrit - (SIZE / 2)];
    }
    for (size_t numOfTrit = 0; numOfTrit < SIZE; ++numOfTrit) {
        EXPECT_EQ(Trit::True, set[numOfTrit]);
    }
}

TEST(TestsOfTritsetOperators, LogicalOperatorANDTest) {
    Tritset setA{9};
    setA[2] = Trit::False;
    setA[4] = Trit::True;
    setA[5] = Trit::True;
    setA[6] = Trit::True;
    setA[7] = Trit::False;
    setA[8] = Trit::False;
    Tritset setB{9};
    setB[0] = Trit::Unknown;
    setB[1] = Trit::False;
    setB[2] = Trit::Unknown;
    setB[3] = Trit::True;
    setB[4] = Trit::Unknown;
    setB[5] = Trit::True;
    setB[6] = Trit::False;
    setB[7] = Trit::True;
    setB[8] = Trit::False;
    Tritset setC = setA & setB;
    EXPECT_TRUE(setC[0] == Trit::Unknown);
    EXPECT_TRUE(setC[1] == Trit::False);
    EXPECT_TRUE(setC[2] == Trit::False);
    EXPECT_TRUE(setC[3] == Trit::Unknown);
    EXPECT_TRUE(setC[4] == Trit::Unknown);
    EXPECT_TRUE(setC[5] == Trit::True);
    EXPECT_TRUE(setC[6] == Trit::False);
    EXPECT_TRUE(setC[7] == Trit::False);
    EXPECT_TRUE(setC[8] == Trit::False);
}

TEST(TestsOfTritsetOperators, LogicalOperatorORDTest) {
    Tritset setA{9};
    setA[2] = Trit::False;
    setA[4] = Trit::True;
    setA[5] = Trit::True;
    setA[6] = Trit::True;
    setA[7] = Trit::False;
    setA[8] = Trit::False;
    Tritset setB{9};
    setB[0] = Trit::Unknown;
    setB[1] = Trit::False;
    setB[2] = Trit::Unknown;
    setB[3] = Trit::True;
    setB[4] = Trit::Unknown;
    setB[5] = Trit::True;
    setB[6] = Trit::False;
    setB[7] = Trit::True;
    setB[8] = Trit::False;
    Tritset setC = setA | setB;
    EXPECT_TRUE(setC[0] == Trit::Unknown);
    EXPECT_TRUE(setC[1] == Trit::Unknown);
    EXPECT_TRUE(setC[2] == Trit::Unknown);
    EXPECT_TRUE(setC[3] == Trit::True);
    EXPECT_TRUE(setC[4] == Trit::True);
    EXPECT_TRUE(setC[5] == Trit::True);
    EXPECT_TRUE(setC[6] == Trit::True);
    EXPECT_TRUE(setC[7] == Trit::True);
    EXPECT_TRUE(setC[8] == Trit::False);
}

TEST(TestsOfTritsetOperators, LogicalOperatorNOTTest) {
    Tritset setA{3};
    setA[0] = Trit::Unknown;
    setA[1] = Trit::False;
    setA[2] = Trit::True;
    Tritset setB = ~setA;
    EXPECT_TRUE(setB[0] == Trit::Unknown);
    EXPECT_TRUE(setB[1] == Trit::True);
    EXPECT_TRUE(setB[2] == Trit::False);
}

TEST(TestsOfTritsetMethods, ShinkTest) {
    Tritset set{100};
    size_t allocLenght = set.Capacity();
    set[12] = Trit::False;
    set.Shrink();
    EXPECT_GT(allocLenght, set.Capacity());
    allocLenght = set.Capacity();
    set[200] = Trit::Unknown;
    set.Shrink();
    EXPECT_EQ(allocLenght, set.Capacity());
    set[100] = Trit::True;
    set.Shrink();
    EXPECT_LT(allocLenght, set.Capacity());
}

TEST(TestsOfTritsetMethods, CardinalityTest_1) {
    Tritset set{12};
    set[0] = Trit::True;
    set[1] = Trit::Unknown;
    set[2] = Trit::False;
    set[3] = Trit::Unknown;
    set[4] = Trit::True;
    set[5] = Trit::False;
    set[6] = Trit::True;
    set[7] = Trit::Unknown;
    set[8] = Trit::False;
    set[9] = Trit::Unknown;
    set[10] = Trit::False;
    set[11] = Trit::False;
    EXPECT_EQ(set.Cardinality(Trit::True), 3);
    EXPECT_EQ(set.Cardinality(Trit::Unknown), 4);
    EXPECT_EQ(set.Cardinality(Trit::False), 5);
}


TEST(TestsOfTritsetMethods, CardinalityTest_2) {
    const size_t SIZE = 150;
    Tritset set{SIZE};
    const size_t NUM_OF_EACH_EL = SIZE / NUM_OF_EL_IN_ENUM;
    for (size_t numOfTrit = 0; numOfTrit < NUM_OF_EACH_EL; ++numOfTrit) {
        set[numOfTrit] = Trit::True;
    }
    for (size_t numOfTrit = NUM_OF_EACH_EL; numOfTrit <  NUM_OF_EACH_EL * (NUM_OF_EL_IN_ENUM - 1); ++numOfTrit) {
        set[numOfTrit] = Trit::Unknown;
    }
    for (size_t numOfTrit =  NUM_OF_EACH_EL * (NUM_OF_EL_IN_ENUM - 1); numOfTrit < SIZE; ++numOfTrit) {
        set[numOfTrit] = Trit::False;
    }
    EXPECT_EQ(set.Cardinality(Trit::True), NUM_OF_EACH_EL);
    EXPECT_EQ(set.Cardinality(Trit::False), NUM_OF_EACH_EL);
    EXPECT_EQ(set.Cardinality(Trit::Unknown), NUM_OF_EACH_EL);
}

TEST(TestsOfTritsetMethods, CardinalityTest_3) {
    const size_t SIZE = 150;
    Tritset set{SIZE};
    const size_t NUM_OF_EACH_EL = SIZE / NUM_OF_EL_IN_ENUM;
    for (size_t numOfTrit = 0; numOfTrit < NUM_OF_EACH_EL; ++numOfTrit) {
        set[numOfTrit] = Trit::True;
    }
    for (size_t numOfTrit = NUM_OF_EACH_EL; numOfTrit <  NUM_OF_EACH_EL * (NUM_OF_EL_IN_ENUM - 1); ++numOfTrit) {
        set[numOfTrit] = Trit::Unknown;
    }
    for (size_t numOfTrit =  NUM_OF_EACH_EL * (NUM_OF_EL_IN_ENUM - 1); numOfTrit < SIZE; ++numOfTrit) {
        set[numOfTrit] = Trit::False;
    }
    const size_t lastIndex = 70;
    set.Trim(lastIndex);
    EXPECT_EQ(set.Cardinality(Trit::True), NUM_OF_EACH_EL);
    EXPECT_EQ(set.Cardinality(Trit::False), 0);
    EXPECT_EQ(set.Cardinality(Trit::Unknown), SIZE - lastIndex);
}

TEST(TestsOfTritsetMethods, CardinalityMapTest_1) {
    Tritset set{12};
    set[0] = Trit::True;
    set[1] = Trit::Unknown;
    set[2] = Trit::False;
    set[3] = Trit::Unknown;
    set[4] = Trit::True;
    set[5] = Trit::False;
    set[6] = Trit::True;
    set[7] = Trit::Unknown;
    set[8] = Trit::False;
    set[9] = Trit::Unknown;
    set[10] = Trit::False;
    set[11] = Trit::False;
    std::unordered_map<Trit, int , Tritset::TritHasher> equalMap {
            {Trit::True, 3},
            {Trit::False, 5},
            {Trit::Unknown, 4}
    };
    EXPECT_EQ(set.Cardinality(), equalMap);
}

TEST(TestsOfTritsetMethods, CardinalityMapTest_2) {
    const size_t SIZE = 150;
    Tritset set{SIZE};
    const size_t NUM_OF_EACH_EL = SIZE / NUM_OF_EL_IN_ENUM;
    for (size_t numOfTrit = 0; numOfTrit < NUM_OF_EACH_EL; ++numOfTrit) {
        set[numOfTrit] = Trit::True;
    }
    for (size_t numOfTrit = NUM_OF_EACH_EL; numOfTrit <  NUM_OF_EACH_EL * (NUM_OF_EL_IN_ENUM - 1); ++numOfTrit) {
        set[numOfTrit] = Trit::Unknown;
    }
    for (size_t numOfTrit =  NUM_OF_EACH_EL * (NUM_OF_EL_IN_ENUM - 1); numOfTrit < SIZE; ++numOfTrit) {
        set[numOfTrit] = Trit::False;
    }
    std::unordered_map<Trit, int , Tritset::TritHasher> equalMap {
            {Trit::True, NUM_OF_EACH_EL},
            {Trit::False, NUM_OF_EACH_EL},
            {Trit::Unknown, NUM_OF_EACH_EL}
    };
    EXPECT_EQ(set.Cardinality(), equalMap);
}

TEST(TestsOfTritsetMethods, CardinalityMapTest_3) {
    const size_t SIZE = 150;
    Tritset set{SIZE};
    const size_t NUM_OF_EACH_EL = SIZE / NUM_OF_EL_IN_ENUM;
    for (size_t numOfTrit = 0; numOfTrit < NUM_OF_EACH_EL; ++numOfTrit) {
        set[numOfTrit] = Trit::True;
    }
    for (size_t numOfTrit = NUM_OF_EACH_EL; numOfTrit <  NUM_OF_EACH_EL * (NUM_OF_EL_IN_ENUM - 1); ++numOfTrit) {
        set[numOfTrit] = Trit::Unknown;
    }
    for (size_t numOfTrit =  NUM_OF_EACH_EL * (NUM_OF_EL_IN_ENUM - 1); numOfTrit < SIZE; ++numOfTrit) {
        set[numOfTrit] = Trit::False;
    }
    const size_t lastIndex = 70;
    set.Trim(lastIndex);
    std::unordered_map<Trit, int , Tritset::TritHasher> equalMap {
            {Trit::True, NUM_OF_EACH_EL},
            {Trit::False, 0},
            {Trit::Unknown, SIZE - lastIndex}
    };
}

TEST(TestsOfTritsetMethods, TrimTest_1) {
    const size_t SIZE = 100;
    Tritset set{SIZE};
    const size_t last_index = 90;
    for (size_t numOfTrit = last_index; numOfTrit < (SIZE / 2); ++numOfTrit) {
        set[numOfTrit] = Trit::True;
    }
    for (size_t numOfTrit = (SIZE / 2); numOfTrit < SIZE; ++numOfTrit) {
        set[numOfTrit] = Trit::False;
    }
    set.Trim(last_index);
    for (short numOfTrit = last_index; numOfTrit < SIZE; ++numOfTrit) {
        EXPECT_EQ(set[numOfTrit], Trit::Unknown);
    }
}

TEST(TestsOfTritsetMethods, TrimTest_2) {
    const size_t SIZE = 100;
    Tritset set{SIZE};
    const size_t last_index = 0;
    for (size_t numOfTrit = last_index; numOfTrit < (SIZE / 2); ++numOfTrit) {
        set[numOfTrit] = Trit::True;
    }
    for (size_t numOfTrit = (SIZE / 2); numOfTrit < SIZE; ++numOfTrit) {
        set[numOfTrit] = Trit::False;
    }
    set.Trim(last_index);
    for (short numOfTrit = last_index; numOfTrit < SIZE; ++numOfTrit) {
        EXPECT_EQ(set[numOfTrit], Trit::Unknown);
    }
}

TEST(TestsOfTritsetMethods, LenghtTest_1) {
    const size_t SIZE = 100;
    Tritset set{SIZE};
    set[15] = Trit::True;
    set[21] = Trit::True;
    set[32] = Trit::True;
    set[40] = Trit::True;
    set[5] = Trit::True;
    set[3] = Trit::False;
    set[65] = Trit::False;
    set[12] = Trit::False;
    set[14] = Trit::False;
    set[65] = Trit::Unknown;
    EXPECT_EQ(set.Length(), 41);
}

TEST(TestsOfTritsetMethods, LenghtTest_2) {
    const size_t SIZE = 150;
    Tritset set{SIZE};
    EXPECT_EQ(set.Length(), 0);
}

TEST(TestsOfTritsetMethods, LenghtTest_3) {
    const size_t SIZE = 150;
    Tritset set{SIZE};
    for (size_t numOfTrit = 0; numOfTrit < SIZE; ++numOfTrit) {
        if (numOfTrit % NUM_OF_EL_IN_ENUM == ZERO_EL) {
            set[numOfTrit] = Trit::Unknown;
        }
        if (numOfTrit % NUM_OF_EL_IN_ENUM == FIRST_EL) {
            set[numOfTrit] = Trit::True;
        }
        if (numOfTrit % NUM_OF_EL_IN_ENUM == SECOND_EL) {
            set[numOfTrit] = Trit::False;
        }
    }
    size_t index = 100;
    size_t  prevIndex = index - 1;
    set.Trim(index);
    if (prevIndex % NUM_OF_EL_IN_ENUM == ZERO_EL) {
        EXPECT_EQ(set.Length(), prevIndex);
    }
    if (prevIndex % NUM_OF_EL_IN_ENUM == FIRST_EL) {
        EXPECT_EQ(set.Length(), prevIndex + 1);
    }
    if (prevIndex % NUM_OF_EL_IN_ENUM == SECOND_EL) {
        EXPECT_EQ(set.Length(), prevIndex + 1);
    }
}

TEST(TestsOfTritsetMethods, LenghtTest_4) {
    const size_t SIZE = 100;
    Tritset set{SIZE};
    size_t newSize = (SIZE / 2);
    for (ull numOfTrit = 0; numOfTrit < newSize; ++numOfTrit) {
        set[numOfTrit] = Trit::True;
    }
    set.Shrink();
    EXPECT_EQ(set.Length(), newSize);
}

