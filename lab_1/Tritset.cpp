#include "Tritset.h"
#include "Constants.h"
#include <iostream>
#include "Proxy.h"

using Constants::TRIT_BIT_SIZE;
using Constants::ULL_BIT_SIZE;
using Constants::FIRST_BIT;
using Constants::BINARY_BASE;
using Constants::BIT_SIZE;
using Constants::NOT_A_PLACE;
using Constants::NULL_LENGTH;

namespace TritSetSpace {

    Tritset::Tritset(size_t numOfTrits) : numOfTrits(numOfTrits) {
        placeOfLastSetTrit = numOfTrits - 1;
        placeLastNotUnknown = NOT_A_PLACE;
        values.resize(GetNumOfUll(numOfTrits), static_cast<ull>(Trit::Unknown));
    }

    size_t Tritset::GetNumOfUll(size_t curNumOfTrits) {
        return ((curNumOfTrits * TRIT_BIT_SIZE - TRIT_BIT_SIZE) + ULL_BIT_SIZE) / ULL_BIT_SIZE;
    }

    uchar Tritset::GetBit(ull val, ushort place) {
        return !!(val & (static_cast<ull>(FIRST_BIT) << place));
    }

    Trit Tritset::GetTrit(size_t tritNum) const {
        ull offset = (tritNum * TRIT_BIT_SIZE + TRIT_BIT_SIZE) / ULL_BIT_SIZE; // index of current ull
        ull effset = (tritNum * TRIT_BIT_SIZE + TRIT_BIT_SIZE) % ULL_BIT_SIZE; // bit index in current ull
        ushort bitPlace = (ULL_BIT_SIZE - effset) % ULL_BIT_SIZE;
        return static_cast<Trit>(GetBit(values[offset], bitPlace + BIT_SIZE) * BINARY_BASE +
                                 GetBit(values[offset], bitPlace));
    }

    void Tritset::Shrink() {
        values.resize(GetNumOfUll(placeOfLastSetTrit + 1));
        FixSize();
        FixPlaceLastNotUnknown();
    }

    size_t Tritset::Cardinality(Trit value) {
        size_t numOfValue = 0;
        for (int numOfTrit = 0; numOfTrit < numOfTrits; ++numOfTrit) {
            if (GetTrit(numOfTrit) == value) {
                ++numOfValue;
            }
            if (value == Trit::Unknown && numOfTrit == placeOfLastSetTrit) {
                break;
            }
        }
        return numOfValue;
    }

    size_t Tritset::TritHasher::operator()(const Trit &t) const {
        return std::hash<int>()(static_cast<int>(t));
    }

    std::unordered_map<Trit, int, Tritset::TritHasher> Tritset::Cardinality() {
        std::unordered_map<Trit, int, Tritset::TritHasher> resultMap {
                {Trit::True, Cardinality(Trit::True)},
                {Trit::False, Cardinality(Trit::False)},
                {Trit::Unknown, Cardinality(Trit::Unknown)}
        };
        return resultMap;
    }

    void Tritset::Trim(size_t lastIndex) {
        Proxy setter{*this, lastIndex};
        setter.SetTrits(Trit::Unknown, numOfTrits - lastIndex);
        values.resize(GetNumOfUll(lastIndex));
        numOfTrits = lastIndex;
        FixPlaceLastNotUnknown();
    }

    size_t Tritset::Length() const {
        if (placeLastNotUnknown == NOT_A_PLACE) {
            return NULL_LENGTH;
        }
        return placeLastNotUnknown + 1;
    }

    Proxy Tritset::operator[](size_t tritNum) {
        if (tritNum > numOfTrits) {
            return Proxy{*this, tritNum};
        }
        return Proxy{*this, tritNum, GetTrit(tritNum)};
    }

    size_t Tritset::Capacity() const {
        return values.size();
    }

    size_t Tritset::SearchLastNotUnknow() {
        size_t resPlace = NOT_A_PLACE;
        for (int numOfTrit = 0; numOfTrit < numOfTrits; ++numOfTrit) {
            if ((*this)[numOfTrit] != Trit::Unknown) {
                resPlace = numOfTrit;
            }
        }
        return resPlace;
    }

    void Tritset::FixSize() {
        if (placeOfLastSetTrit > numOfTrits - 1) {
            numOfTrits = placeOfLastSetTrit + 1;
        }
    }

    void Tritset::FixPlaceLastNotUnknown() {
        if (numOfTrits < placeLastNotUnknown) {
            placeLastNotUnknown = SearchLastNotUnknow();
        }
    }

    Tritset Tritset::operator~() {
        Tritset newTritset{numOfTrits};
        for (int numOfTrit = 0; numOfTrit < numOfTrits; ++numOfTrit) {
            newTritset[numOfTrit] = ~(*this)[numOfTrit];
        }
        return newTritset;
    }

    Tritset Tritset::operator|(Tritset set) {
        ull newNumOfTrits = std::max(numOfTrits, set.numOfTrits);
        Tritset newTritset{newNumOfTrits};
        for (int numOfTrit = 0; numOfTrit < newNumOfTrits; ++numOfTrit) {
            Trit first = (*this)[numOfTrit];
            Trit second = set[numOfTrit];
            newTritset[numOfTrit] = first | second;
        }
        return newTritset;
    }

    Tritset Tritset::operator&(Tritset set) {
        ull newNumOfTrits = std::max(numOfTrits, set.numOfTrits);
        Tritset newTritset{newNumOfTrits};
        for (int numOfTrit = 0; numOfTrit < newNumOfTrits; ++numOfTrit) {
            Trit first = (*this)[numOfTrit];
            Trit second = set[numOfTrit];
            newTritset[numOfTrit] = first & second;
        }
        return newTritset;
    }

}