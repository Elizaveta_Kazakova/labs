#ifndef LAB_1_PROXY_H
#define LAB_1_PROXY_H

#include "Trit.h"
#include "Tritset.h"
#include <vector>

typedef unsigned long long ull;
typedef unsigned short ushort;
typedef unsigned char uchar;

namespace TritSetSpace {

    class Proxy {
    private:
        Tritset &tritset;
        Trit trit;
        size_t tritNum;
        static void SetBit(ull &bits, uchar val, ushort place);

        void SetTrit(Trit val);

        void FixVectorSize(ull curIndex);

        void FixPlaceLastNotUnknown(Trit newVal);

    public:
        void SetTrits(Trit val, size_t numOfTrits);

        operator Trit() const;

        Proxy(Tritset &tritset, size_t num, Trit curTrit = Trit::Unknown);

        void operator=(Trit value);

        Proxy &operator=(Proxy anotherProxy);

    };
}

#endif //LAB_1_PROXY_H
